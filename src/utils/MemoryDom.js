class MemoryDom {
    
    __node = null;
    __fn = null;
    
    constructor(htmlStream) {
       this.__node = document.createElement('span');
       this.__node.innerHTML = htmlStream;
    }

    get node() {
        return this.__node;
    }

    get htmlStream() {
        return this.__node === null ? null : this.__node.innerHTML;
    }

    traverse(node, parentNode = null, depth = -1) {
        
        if(depth > -1) {
            this.__fn.call(this, node, parentNode || node, depth);
        }

        let { childNodes } = node;

        if (childNodes.length) {
            let i = 0;

            do {
                this.traverse(childNodes[i], node, depth + 1);
            } while (++i < childNodes.length);
        }
    }

    forEach(fn) {
        if(this.__node === null || fn === null){
            return;
        }

        this.__fn = fn;
        this.traverse(this.__node);
        this.__fn = null;
    }
    
}

export default MemoryDom;
