import React, { Component, createRef } from 'react';

import highlightText from './utils/highlightText';

import './App.css';


class App extends Component {

  textRef;

  constructor(props) {
    super(props);

    this.textRef = createRef();

    this.state = {
      text: '<p>hello world<!--help hello--> <span>hellowor</span><div>HellOxx</div>ld</p>'
    }
  }
  componentDidMount(){
    let highlightedNode = highlightText(this.state.text, 'hello');
    this.textRef.current.appendChild(highlightedNode);
  }

  render() {
    return (
      <div className="App" >
        <span>{this.state.text}</span>
        <span dangerouslySetInnerHTML={{__html: this.state.text}} />
        <span ref={this.textRef} />
      </div>
    );
  }
}

export default App;
