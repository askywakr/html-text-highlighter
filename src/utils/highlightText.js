import MemoryDom from "./MemoryDom";

const highlightText = (textStream, highlightText) => {

    let memDom = new MemoryDom(textStream);

    memDom.forEach( (node, parentNode)  => {
        let {nodeType, nodeValue} = node;

        if(nodeType === 3) {

            let pattern = new RegExp(highlightText, 'ig');

            if(pattern.test(nodeValue)) {
                let matches = nodeValue.match(pattern);
                let i = 0;

                do{
                    nodeValue = nodeValue.replace(matches[i], `<mark>${matches[i]}</mark>`);
                } while(++i < matches.length);
                

                let highlightedNode = (new MemoryDom(nodeValue)).node;

                parentNode.replaceChild(highlightedNode, node);
            }
        }
    });

    return memDom.node;
}

export default highlightText;